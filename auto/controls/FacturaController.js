'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models/');
var rol = models.rol;
var factura = models.factura;
const bcrypt = require('bcrypt');
const saltRounds = 8;

class FacturaController {
    async listar(req, res) {
        var lista = await factura.findAll({
            //include: { model: models.cuenta, as: "cuenta", attributes: ['correo'] },
            attributes: ['numfact', 'motodpago', 'fecha', 'total']
        });
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async obtener(req, res) {
        const external = req.params.external;
        var lista = await auto.findOne({
            where: { external_id: external }, include: { model: models.cuenta, as: "cuenta", attributes: ['correo'] },
            attributes: ['modelo', 'color', 'anio', 'marca', 'placa', 'motor', 'external_id', 'precio']
        });
        if (lista === null) {
            lista = {};
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            let marca_id = req.body.external_marca;
            if (marca_id != undefined) {
                let marcaAux = await marca.findOne({ where: { external_id: marca_id } });
                if (marcaAux) {
                    var data = {
                        modelo: req.body.modelo,
                        color: req.body.color,
                        anio: req.body.anio,
                        placa: req.body.placa,
                        motor: req.body.motor,
                        precio: req.body.precio,
                        id_marca: marcaAux.id,
                    };
                    res.status(200);
                    let transaction = await models.sequelize.transaction();
                    try {
                        await auto.create(data, transaction);
                        res.json({ msg: "Se han registrado sus datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 })
                        } else {
                            res.json({ msg: error.message, code: 200 })
                        }

                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Datos no encontrados", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400 });
        }

    }

    async modificar(req, res) {
        var aut = await auto.findOne({ where: { external_id: req.body.external } })
        if (aut === null) {
            res.json({ msg: "no existe el registro", code: 400 });
        } else {
            var uuid = require('uuid');
            aut.identificacion = req.body.dni;
            aut.tipo_identificacion = req.body.tipo;
            aut.apellidos = req.body.apellidos;
            aut.nombres = req.body.nombres;
            aut.direccion = req.body.direccion;
            aut.external_id = uuid.v4();
            var result = await aut.save();
            if (result === null) {
                res.status(400);
                res.json({ msg: "No se han modificado sus datos", code: 200 });
            } else {
                res.status(200);
                res.json({ msg: "Se han modificado sus datos", code: 200 });
            }
        }
    }
}

module.exports = FacturaController;