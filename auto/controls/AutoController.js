'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models/');
var auto = models.auto;


class AutoController {
    async listar(req, res) {
        var lista = await auto.findAll({
            attributes: ['modelo', 'color','anio','placa','motor','external_id','precio']
        });
        res.json({msg: "OK!", code: 200, info: lista});
    }
    async numAuto(req, res) {
        var num = await auto.count();
        res.json({ msg: "OK!", code: 200, marcas: num });
    }
    

    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            let marca_id = req.body.external_marca;
            if (marca_id != undefined) {
                let marcaAux = await models.marca.findOne({ where: { external_id: marca_id } });
                console.log("4rfr4tfgtr"+marca_id);
                if (marcaAux) {
                    var data = {
                        modelo: req.body.modelo,
                        color: req.body.color,
                        anio: req.body.anio,
                        placa: req.body.placa,
                        motor: req.body.motor,
                        precio: req.body.precio,
                        id_marca: marcaAux.id,
                    };
                    console.log(data)
                    res.status(200);
                    let transaction = await models.sequelize.transaction();
                    try {
                        await auto.create(data, transaction);
                        res.json({ msg: "Se han registrado sus datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 })
                        } else {
                            res.json({ msg: error.message, code: 200 })
                        }

                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Datos no encontrados", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400 });
        }

    }
}

module.exports = AutoController;