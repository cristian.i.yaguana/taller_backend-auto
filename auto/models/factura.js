'use strict';

module.exports = (sequelize, DataTypes) => {
    const factura = sequelize.define('factura', {
        numfact: {type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        motodpago: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        fecha: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
        total: { type: DataTypes.DOUBLE, defaultValue: 0.0 },
        estado: { type: DataTypes.BOOLEAN, defaultValue: true }
    }, { freezeTableName: true });

    //persona.associate = function(models){
    //    persona.belongsTo(models.rol, {foreignKey: 'id_rol'});
     //   persona.hasOne(models.cuenta,{foreignKey: 'id_persona', as: 'cuenta'});
    //}
    return factura;
};