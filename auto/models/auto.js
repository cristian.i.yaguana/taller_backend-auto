'use strict';

module.exports = (sequelize, DataTypes) => {
    const auto = sequelize.define('auto', {
        modelo: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        color: {type: DataTypes.ENUM('BLANCO','ROJO','NEGRO','AZUL','VERDE','MORADO','PLATA','MULTICOLOR'), defaultValue: 'BLANCO'},
        anio: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        placa: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        motor: { type: DataTypes.ENUM("DIESEL", "ELECTRICO", "GASOLINA"), defaultValue: "GASOLINA" },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
        precio: { type: DataTypes.DOUBLE, defaultValue: 0.0 },
        estado: { type: DataTypes.BOOLEAN, defaultValue: false }
    }, { freezeTableName: true });

    auto.associate = function(models){
        auto.belongsTo(models.marca, {foreignKey: 'id_marca'});
        auto.belongsTo(models.det_factura, {foreignKey: 'det_factura_id'});
    }
    return auto;
};