'use strict';

module.exports = (sequelize, DataTypes) => {
    const det_factura = sequelize.define('det_factura', {
        cantidad: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        descripcion: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        preciounit: { type: DataTypes.DOUBLE, defaultValue: 0.0 },
        subtotal: { type: DataTypes.DOUBLE, defaultValue: 0.0 },
        
    }, { freezeTableName: true });

    //persona.associate = function(models){
    //    persona.belongsTo(models.rol, {foreignKey: 'id_rol'});
     //   persona.hasOne(models.cuenta,{foreignKey: 'id_persona', as: 'cuenta'});
    //}
    return det_factura;
};