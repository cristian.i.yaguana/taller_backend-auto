
'use strict';
const { UUIDV4, DOUBLE } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    const marca = sequelize.define('marca', {
        nommarca: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        distribuidor: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
        estado: { type: DataTypes.BOOLEAN, defaultValue: true }
        
    }, {
        freezeTableName: true
    });
    marca.associate = function (models){
        marca.hasMany(models.auto, {foreignKey: 'id_marca', as: 'auto'});
    }

    return marca;
};

